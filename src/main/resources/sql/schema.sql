CREATE TABLE orders (
  id BIGINT NOT NULL PRIMARY KEY IDENTITY,
  order_number VARCHAR (255)
);

CREATE TABLE order_items (
  id BIGINT NOT NULL PRIMARY KEY IDENTITY,
  order_id BIGINT NOT NULL,
  item_name VARCHAR (255),
  quantity INT,
  price INT,
  FOREIGN KEY (order_id) REFERENCES orders (id)
);

--INSERT INTO orders (order_number) values ('asdf');
--INSERT INTO order_items (order_id, item_name, quantity, price) values (0, 'test', 1, 10);