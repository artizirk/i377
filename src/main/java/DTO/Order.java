package DTO;
import Exceptions.ValidationException;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

public @Data class Order {
    private int id;
    private String orderNumber;
    private List<OrderItem> orderRows = new LinkedList<>();
}
