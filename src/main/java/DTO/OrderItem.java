package DTO;

import lombok.Data;

public @Data class OrderItem {
    private String itemName;
    private Integer quantity;
    private Integer price;
}
