package DTO;

import lombok.Data;

@Data
public class Report {

    private Integer count;
    private Integer averageOrderAmount;
    private Integer turnoverWithoutVAT;
    private Integer turnoverVAT;
    private Integer turnoverWithVAT;

}
