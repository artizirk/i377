package DTO;

import lombok.Data;
import java.util.List;

@Data
public class ValidationError {
    private String code;
    private List<String> arguments;
}
