package DTO;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class ValidationErrors {

    private List<ValidationError> errors = new LinkedList<>();

    public void addError(ValidationError error) {
        errors.add(error);
    }
}
