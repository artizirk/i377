package servlet.html;

import DAO.OrdersDao;
import DTO.Order;
import DTO.ValidationError;
import DTO.ValidationErrors;
import Exceptions.ValidationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import servlet.api.Orders;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/orders/form", name="OrdersForm")
public class OrdersForm extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html; charset=utf-8");
        resp.getWriter().print(
                "<form  method=post>" +
                "<label for=orderNumber>Order Number</label> " +
                "<input type=text name=orderNumber></input> " +
                "<input type=submit></input>" +
                "</form>"
        );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        String orderNumber = req.getParameter("orderNumber");
        if (orderNumber == null) {
            resp.setStatus(400, "orderNumber parameter is empty");
            resp.getWriter().print("orderNumber parameter is empty");
            return;
        }

        Order order = new Order();
        order.setOrderNumber(orderNumber);
        if (order.getOrderNumber().length() < 2) {
            resp.setStatus(400, "orderNumber parameter is empty");
            resp.setContentType("application/json");
            ValidationErrors errors = new ValidationErrors();
            ValidationError error = new ValidationError();
            error.setCode("too_short_number");
            errors.addError(error);
            resp.getWriter().print(new ObjectMapper().writeValueAsString(errors));
            return;
        }
        OrdersDao.saveOrder(order);
        resp.getWriter().format("%d", order.getId());
    }
}
