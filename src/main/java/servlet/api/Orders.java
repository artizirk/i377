package servlet.api;

import DAO.OrdersDao;
import DTO.Order;
import DTO.ValidationError;
import DTO.ValidationErrors;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/api/orders", name="Orders")
public class Orders extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        String idString = req.getParameter("id");
        if (idString != null) {
            int id = Integer.parseInt(idString);
            Order order = OrdersDao.getOrderForId(id);
            if (order == null) {
                resp.setStatus(404);
                return;
            }
            resp.getWriter().print(new ObjectMapper().writeValueAsString(order));
            return;
        }
        List<Order> orders = OrdersDao.getAllOrders();
        resp.getWriter().print(new ObjectMapper().writeValueAsString(orders));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        String orderJson = req.getReader().lines().collect(Collectors.joining(" "));

        Order order = new ObjectMapper().readValue(orderJson, Order.class);

        if (order.getOrderNumber().length() < 2) {
            resp.setStatus(400, "orderNumber parameter is empty");
            resp.setContentType("application/json");
            ValidationErrors errors = new ValidationErrors();
            ValidationError error = new ValidationError();
            error.setCode("too_short_number");
            errors.addError(error);
            resp.getWriter().print(new ObjectMapper().writeValueAsString(errors));
            return;
        }

        OrdersDao.saveOrder(order);
        resp.getWriter().print(new ObjectMapper().writeValueAsString(order));

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        String idString = req.getParameter("id");
        if (idString != null) {
            int id = Integer.parseInt(idString);
            OrdersDao.deleteOrderForId(id);
        } else {
            OrdersDao.deleteAllOrders();
        }
    }
}
