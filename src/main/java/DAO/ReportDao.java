package DAO;

import DTO.Order;
import DTO.OrderItem;
import DTO.Report;

public class ReportDao {
    public static Report getReport() {
        Report report = new Report();
        report.setCount(OrdersDao.getCount());
        report.setTurnoverWithoutVAT(0);
        int itemCount = 0;
        for (Order order: OrdersDao.getAllOrders()) {
            for (OrderItem orderItem: order.getOrderRows()) {
                int turnover = report.getTurnoverWithoutVAT() + (orderItem.getPrice() * orderItem.getQuantity());
                report.setTurnoverWithoutVAT(turnover);
                itemCount++;
            }
        }
        report.setAverageOrderAmount(report.getTurnoverWithoutVAT() / itemCount);
        report.setTurnoverVAT((int)(report.getTurnoverWithoutVAT() * 0.2));
        report.setTurnoverWithVAT(report.getTurnoverVAT() + report.getTurnoverWithoutVAT());
        return report;
    }
}
