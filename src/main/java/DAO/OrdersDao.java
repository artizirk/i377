package DAO;

import DTO.Order;
import Exceptions.ValidationException;
import util.DataSourceProvider;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrdersDao {

    public static Order getOrderForId(int id) {
        Order order = new Order();
        String sql = "select order_number from ORDERS where id=?;";
        try (Connection connection = DataSourceProvider.getDataSource().getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    order.setId(id);
                    order.setOrderNumber(rs.getString(1));

                    order.setOrderRows(OrderItemDao.getItemsForOrderId(id));
                    return order;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }
    public static List<Order> getAllOrders() {
        List<Order> orders = new ArrayList<Order>();
        String sql = "select id, order_number from orders;";
        try (Connection connection = DataSourceProvider.getDataSource().getConnection();
             Statement statement = connection.createStatement() ){

            try (ResultSet rs = statement.executeQuery(sql)) {
                while(rs.next()) {
                    Order order = new Order();
                    order.setId(rs.getInt(1));

                    order.setOrderNumber(rs.getString(2));

                    order.setOrderRows(OrderItemDao.getItemsForOrderId(order.getId()));
                    orders.add(order);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return orders;
    }

    public static void saveOrder(Order order) {
        String sql = "insert into orders (order_number) values (?);";
        try (Connection connection = DataSourceProvider.getDataSource().getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             PreparedStatement call = connection.prepareCall("CALL IDENTITY();")) {
            ps.setString(1, order.getOrderNumber());
            ps.executeUpdate();
            try (ResultSet rs = call.executeQuery()) {
                if (rs.next()) {
                    order.setId(rs.getInt(1));
                    OrderItemDao.saveOrderItems(order.getOrderRows(), order.getId());
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void deleteOrderForId(int id) {
        String sql = "DELETE FROM orders WHERE id=?;";
        try (Connection connection = DataSourceProvider.getDataSource().getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, id);
            ps.executeUpdate();
            OrderItemDao.deleteOrderItemsForOrderId(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static int getCount() {
        String sql = "select count(*) from orders;";
        try (Connection connection = DataSourceProvider.getDataSource().getConnection();
             Statement ps = connection.createStatement()) {
            try (ResultSet rs = ps.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public static void deleteAllOrders() {
        String sql = "delete from orders;";
        try (Connection connection = DataSourceProvider.getDataSource().getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            OrderItemDao.deleteAllOrderItems();
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}