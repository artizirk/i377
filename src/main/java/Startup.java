import util.DataSourceProvider;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static util.FileUtil.readFileFromClasspath;

@WebListener
public class Startup implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Set jdbc provider url");
        DataSourceProvider.setDbUrl("jdbc:hsqldb:mem:shop.");

        System.out.println("Load sql");
        String sql = readFileFromClasspath("sql/schema.sql");

        try (Connection connection = DataSourceProvider.getDataSource().getConnection();
            Statement statement = connection.createStatement()) {

            statement.execute(sql);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Ready");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
