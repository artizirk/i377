package Exceptions;

import DTO.ValidationError;
import lombok.Getter;
import lombok.Setter;

public class ValidationException extends Exception {
    @Getter private ValidationError error = new ValidationError();

    public ValidationException(String code) {
        error.setCode(code);
    }
}
